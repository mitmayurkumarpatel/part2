
package bank;

/**
 *
 * @author mitpa
 */
public class Bank 
{
    private String name;

    public String getBankName() {
        return name;
    }

    public Bank(String name) {
        this.name = name;
    }
    
}
